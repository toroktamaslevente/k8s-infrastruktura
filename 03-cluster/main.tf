data "digitalocean_kubernetes_versions" "k8s_cluster_version" {
  version_prefix = "1.21."
}

resource "digitalocean_kubernetes_cluster" "k8s_cluster" {
  name    = "tamas-cluster"
  region  = "fra1"
  auto_upgrade = true
  version      = data.digitalocean_kubernetes_versions.k8s_cluster_version.latest_version

  maintenance_policy {
    start_time  = "03:00"
    day         = "saturday"
  }

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-2vcpu-4gb"
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 1
  }
}