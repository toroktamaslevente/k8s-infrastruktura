## PHPMyAdmin

This will create a PHPMyAdmin pod and link the mysql.k8s.torok.site domain to it.

Once it is deployed, you should be able to connect to MySQL via https://mysql.k8s.torok.site
using the MySQL admin username and password - which you can get from the DigitalOcean
MySQL Connection Details.
