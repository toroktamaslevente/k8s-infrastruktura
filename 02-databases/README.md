## Databases

This Terraform (TF) module will setup a MySQL database and store the TF state
in the 'Spaces' storage defined earlier at '01-spaces'.

See https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/database_cluster
