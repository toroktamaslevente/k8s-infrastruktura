
##########
# Cert manager
resource kubernetes_namespace cert_manager {
  metadata {
    name = "cert-manager"
  }
}

resource helm_release cert_manager {
  name       = "cert-manager"
  namespace  = kubernetes_namespace.cert_manager.metadata.0.name

  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.5.3"

  set {
    name  = "installCRDs"
    value = true
  }
}


##########
# Traefik Ingress
resource kubernetes_namespace traefik_ingress {
  metadata {
    name = "traefik-ingress"
  }
}

resource digitalocean_loadbalancer traefik_do_loadbalancer {
  name                   = var.loadbalancer_name
  region                 = "fra1"
  redirect_http_to_https = false
  enable_proxy_protocol  = true

  healthcheck {
    check_interval_seconds = 3
    port                   = 32080
    protocol               = "tcp"
  }

  # This is here only because you cannot create a load balancer object without forwarding rules.
  # It will be overwritten by Traefik.
  forwarding_rule {
    entry_port      = 443
    entry_protocol  = "tcp"

    target_port     = 32443
    target_protocol = "tcp"

    tls_passthrough = false
  }

  forwarding_rule {
    entry_port      = 80
    entry_protocol  = "tcp"

    target_port     = 32080
    target_protocol = "tcp"

    tls_passthrough = false
  }
}

resource helm_release traefik_ingress {
  name       = "traefik-ingress"
  namespace  = "traefik-ingress"

  repository = "https://helm.traefik.io/traefik"
  chart      = "traefik"
  version    = "10.6.2"   # chart version, see https://github.com/traefik/traefik-helm-chart/tags

  // see Helm values at https://github.com/traefik/traefik-helm-chart/blob/master/traefik/values.yaml
  set {
    name = "image.tag"
    value = "v2.5.4"  # application version at https://hub.docker.com/_/traefik?tab=tags&page=1&ordering=last_updated
  }

  set {
    name  = "deployment.podAnnotations.prometheus\\.io/scrape"
    value = "true"
    type  = "string"
  }

  set {
    name  = "deployment.podAnnotations.prometheus\\.io/port"
    value = "9100"
    type  = "string"
  }

  set {
    name  = "deployment.podAnnotations.prometheus\\.io/path"
    value = "/metrics"
  }

  # the VPC IP range below was taken from https://cloud.digitalocean.com/networking/vpc?i=db90ec
  values = [
    <<EOT
additionalArguments:
  - --accesslog
  - --api.insecure
  - --entryPoints.web.forwardedHeaders.trustedIPs=127.0.0.1/32,10.114.0.0/20
  - --entryPoints.web.proxyProtocol.trustedIPs=10.114.0.0/20
  - --entrypoints.websecure.http.tls
  - --entryPoints.websecure.proxyProtocol.trustedIPs=10.114.0.0/20
  - --log.level=WARNING
  - --metrics.prometheus.addRoutersLabels=true
  - --ping
  - --providers.kubernetescrd.allowCrossNamespace=true
  - --providers.kubernetesIngress.ingressClass=traefik-cert-manager
EOT
  ]

  set {
    name  = "deployment.kind"
    value = "Deployment"
  }

  set {
    name  = "deployment.replicas"
    value = 1
  }

  set {
    name  = "podDisruptionBudget.enabled"
    value = true
  }

  set {
    name  = "podDisruptionBudget.minAvailable"
    value = 1
  }

  set {
    name  = "autoscaling.enabled"
    value = true
  }

  set {
    name  = "autoscaling.minReplicas"
    value = 1
  }

  set {
    name  = "autoscaling.maxReplicas"
    value = 2
  }

  set {
    name  = "resources.requests.memory"
    value = "128Mi"
  }

  set {
    name  = "resources.requests.cpu"
    value = "50m"
  }

  set {
    name  = "resources.limits.memory"
    value = "256Mi"
  }

  set {
    name  = "resources.limits.cpu"
    value = "500m"
  }

  set {
    name  = "ports.web.nodePort"
    value = 32080
  }

  set {
    name  = "ports.websecure.nodePort"
    value = 32443
  }

  set {
    name  = "service.annotations.kubernetes\\.digitalocean\\.com/load-balancer-id"
    value = digitalocean_loadbalancer.traefik_do_loadbalancer.id
  }

  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/do-loadbalancer-name"
    value = var.loadbalancer_name
  }

  set {
    name  = "service.annotations.service\\.beta\\.kubernetes\\.io/do-loadbalancer-enable-proxy-protocol"
    value = "true"
    type  = "string"
  }
}



############################
# Middlewares

resource kubernetes_manifest https_redirect {
  manifest            = {
    apiVersion       = "traefik.containo.us/v1alpha1"
    kind              = "Middleware"
    metadata          = {
      name            = "redirectscheme"
      namespace       = "traefik-ingress"
    }
    spec = {
      redirectScheme = {
        scheme        = "https"
        permanent     = true
      }
    }
  }
}

resource kubernetes_manifest compress {
  manifest            = {
    apiVersion       = "traefik.containo.us/v1alpha1"
    kind              = "Middleware"
    metadata          = {
      name            = "compress"
      namespace       = "traefik-ingress"
    }
    spec = {
      # compress response (if it is above 1400 bytes)
      # see https://doc.traefik.io/traefik/v2.3/middlewares/compress/
      compress = {
        excludedContentTypes = [
          "text/event-stream"
        ]
      }
    }
  }
}

resource kubernetes_manifest limit {
  manifest            = {
    apiVersion       = "traefik.containo.us/v1alpha1"
    kind              = "Middleware"
    metadata          = {
      name            = "limit"
      namespace       = "traefik-ingress"
    }
    spec = {
      # compress response (if it is above 1400 bytes)
      # see https://doc.traefik.io/traefik/v2.3/middlewares/compress/
      buffering = {
        # Sets the maximum request and response body to 2Mb and the max kept in memory to 2Mb    maxRequestBodyBytes: 2000000
        memRequestBodyBytes = 2000000
        maxResponseBodyBytes = 2000000
        memResponseBodyBytes = 2000000
        # retries once in case of a network error:
        # https://doc.traefik.io/traefik/v2.3/middlewares/buffering/#retryexpression
        retryExpression = "IsNetworkError() && Attempts() < 2"
        # for additional options see https://doc.traefik.io/traefik/v2.3/middlewares/buffering/#configuration-options
      }
    }
  }
}
