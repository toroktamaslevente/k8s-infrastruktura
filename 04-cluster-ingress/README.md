## Cluster-ingress

This will setup the ingress into the K8S cluster, which includes
setting up the DigitalOcean LoadBalancer, the Traefik instances within
Kubernetes and then pointing the load balancer to Traefik.

It also includes the intallation of the Certificate Manager.


### How To connect to the Traefik Dashboard

```
# pick up the Traefik pods
$ kubectl get pods --selector "app.kubernetes.io/name=traefik" -n traefik-ingress --output=name
# port-forward to one of those pods
$ kubectl port-forward [podname] 9000:9000 -n traefik-ingress
# open http://localhost:9000
```
