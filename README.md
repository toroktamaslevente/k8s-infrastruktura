# Tamas k8s infra

The DigitalOcean Kubernetes infrastructure for Tamas

## Required software

- doctl ([installation instructions](https://docs.digitalocean.com/reference/doctl/how-to/install/))
- Terraform ([installation instructions](https://www.terraform.io/downloads.html))
- Kubectl ([installation instructions](https://kubernetes.io/docs/tasks/tools/#kubectl))
- Helm ([installation instructions](https://helm.sh/docs/intro/install/))

## Environment variables

When running Terraform, you will have to provide a bunch of values like access tokens and such.
To make your life easier, especially when you run Terraform multiple times, you should have the
following environment variable set (for example in your .bashrc file):

- `DIGITALOCEAN_TOKEN` and `DO_PAT`, with a personal access token generated at
  https://cloud.digitalocean.com/account/api/tokens, 
  see more at https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs#argument-reference
- `SPACES_ACCESS_KEY_ID` and `SPACES_SECRET_ACCESS_KEY`, with a Spaces
  access/secret key pair generated at
  https://cloud.digitalocean.com/account/api/tokens
- `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`, with a Spaces
  access/secret key pair generated at
  https://cloud.digitalocean.com/account/api/tokens
  see more at https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs#argument-reference
<!-- - `TF_VAR_gitlab_ci_registration_token`, with the CI registration token which can be obtained at
  https://gitlab.com/groups/gamesystems/-/settings/ci_cd (you have to open the Runners group)
- `TF_VAR_registry_username` and `TF_VAR_registry_password`, with the Docker credentials for the
  internal registry
- `TF_VAR_spaces_access_key` and `TF_VAR_spaces_secret_key`, with a Spaces access/secret key pair generated at https://cloud.digitalocean.com/account/api/tokens
- `TF_VAR_docker_spaces_access_key` and `TF_VAR_docker_spaces_secret_key`, with a Spaces access/secret key pair generated at https://cloud.digitalocean.com/account/api/tokens
-->

## How to install / uninstall

Different part of the infrastructure has been built via different Terraform modules.
You should follow the order of the directories to set them up - or when uninstalling
then go in the opposite order.

For example you should start with '01-spaces', where we setup the Spaces where
later the Terraform state files will be stored. This one actually is done manually, as this
is a pre-requisite for Terraform.

Second you should go to '02-databases' and so on.

### How to deploy Terraform modules

All Terraform modules are deployed the usual way:
```
$ tf init
$ tf apply
```

To uninstall the given module:
```
$ tf destroy
```

## State location

Global state is configured to be stored in a Spaces bucket in the `fra1` region of DigitalOcean
Spaces.  To access it, you need to specify an access/secret key pair.  You can set the
`AWS_ACCESS_KEY_ID` and `AWS_SECRET_KEY_ID` so you don’t have to provide them.

## How to access the Kubernetes cluster

From the DigitalOcean UI you can download the Kubernetes cluster's config file. You can use this config
file with `kubcetl` to execute `kubectl` commands against the cluster.

By default, `kubectl` looks for a file named `config` in the `$HOME/.kube` directory. You can specify other 
kubeconfig files by setting the `KUBECONFIG` environment variable or by setting the `--kubeconfig` flag.
