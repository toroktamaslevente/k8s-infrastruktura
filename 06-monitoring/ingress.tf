# The certificate to use for the monitoring HTTPS Ingress

resource kubernetes_manifest monitoring_cert {
  manifest        = {
    apiVersion   = "cert-manager.io/v1alpha2"
    kind          = "Certificate"

    metadata      = {
      name        = "monitoring-cert"
      namespace   = kubernetes_namespace.monitoring.metadata.0.name
    }

    spec          = {
      commonName = var.monitoring_fqdn
      secretName = "monitoring-cert"
      dnsNames   = [var.monitoring_fqdn]
      issuerRef  = {
        name      = "letsencrypt"
        kind      = "ClusterIssuer"
      }
    }
  }
}

# The HTTP Ingress
resource kubernetes_manifest grafana_ingress {
  manifest              = {
    apiVersion         = "traefik.containo.us/v1alpha1"
    kind                = "IngressRoute"

    metadata            = {
      name              = "grafana-ingress-https"
      namespace         = kubernetes_namespace.monitoring.metadata.0.name
    }

    spec                = {
      entryPoints      = ["websecure"]
      routes            = [
        {
          match         = "Host(`${var.monitoring_fqdn}`)"
          kind          = "Rule"
          services      = [
            {
              name      = "grafana"
              namespace = "monitoring"
              port      = 80
            }
          ]
          middlewares   = [
            {
              name      = "redirectscheme"
              namespace = "traefik-ingress"
            },
            {
              name      = "compress"
              namespace = "traefik-ingress"
            }
          ]
        }
      ]
      tls = {
        secretName     = kubernetes_manifest.monitoring_cert.manifest.metadata.name
      }
    }
  }
}
