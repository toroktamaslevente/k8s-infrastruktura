
resource helm_release grafana {
  name       = "grafana"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://grafana.github.io/helm-charts"
  chart      = "grafana"
  version    = "6.16.6"

  // see settings at https://github.com/grafana/helm-charts/tree/main/charts/grafana
  set {
    name  = "image.tag"
    value = "8.2.5"  # application version, see $ helm repo update / $ helm search repo grafana
                     # also see at https://hub.docker.com/r/grafana/grafana/tags?page=1&ordering=last_updated
  }

  set {
    name  = "persistence.enabled"
    value = true
  }

  set {
    name  = "persistence.size"
    value = "5Gi"
  }

  set {
    name  = "persistence.storageClassName"
    value = "do-block-storage"
  }

  set {
    name  = "grafana\\.ini.feature_toggles.enable"
    value = "ngalert"
  }

  set {
    name  = "sidecar.dashboards.enabled"
    value = true
  }

  set {
    name  = "sidecar.datasources.enabled"
    value = true
  }

  set {
    name  = "sidecar.notifiers.enabled"
    value = true
  }

  set {
    name  = "grafana\\.init.smtp.enabled"
    value = "false"
  }

#  set {
#    name  = "grafana\\.ini.auth\\.google.enabled"
#    value = "true"
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.client_id"
#    value = var.grafana_google_client_id
#  }
#
#  set_sensitive {
#    name  = "grafana\\.ini.auth\\.google.client_secret"
#    value = var.grafana_google_client_secret
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.scopes"
#    value = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.auth_url"
#    value = "https://accounts.google.com/o/oauth2/auth"
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.token_url"
#    value = "https://accounts.google.com/o/oauth2/token"
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.allowed_domains"
#    value = "benchmark.games"
#  }
#
#  set {
#    name  = "grafana\\.ini.auth\\.google.allow_sign_up"
#    value = "true"
#  }
#
#  set {
#    name  = "grafana\\.init.smtp.enabled"
#    value = "true"
#  }
#
#  set {
#    name  = "grafana\\.ini.smtp.host"
#    value = var.grafana_smtp_host
#  }
#
#  set {
#    name  = "grafana\\.ini.smtp.user"
#    value = var.grafana_smtp_username
#  }
#
#  set {
#    name  = "grafana\\.ini.smtp.password"
#    value = var.grafana_smtp_password
#  }
#
#  set {
#    name  = "grafana\\.ini.smtp.from_address"
#    value = "devops@benchmark.games"
#  }
#
#  set {
#    name  = "grafana\\.ini.smtp.ehlo_identity"
#    value = "${var.env_name}-grafana"
#  }

  set {
    name  = "grafana\\.ini.server.root_url"
    value = "https://${var.monitoring_fqdn}/"
  }
}
