resource helm_release metrics_server {
  name       = "metrics-server"
  namespace  = "kube-system"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"

  set {
    name  = "apiService.create"
    value = true
  }

  set {
    name  = "extraArgs.kubelet-insecure-tls"
    value = true
  }

  set {
    name  = "extraArgs.kubelet-preferred-address-types"
    value = "InternalIP"
  }
}
