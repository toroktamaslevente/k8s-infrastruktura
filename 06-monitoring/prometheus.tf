resource helm_release prometheus-stack {
  name       = "prometheus-stack"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"

  set {
    name  = "grafana.enabled"
    value = false
  }

  set {
    name  = "grafana.forceDeployDashboards"
    value = true
  }

  set {
    name  = "grafana.forceDeployDatasources"
    value = true
  }

  set {
    name  = "kube-state-metrics.namespaceOverride"
    value = "kube-system"
  }

  set {
    name  = "kube-state-metrics.fullnameOverride"
    value = "kube-state-metrics"
  }

  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.storageClassName"
    value = "do-block-storage"
  }

  set {
    name  = "prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues"
    value = false
  }

  set {
    name  = "prometheus.prometheusSpec.storageSpec.volumeClaimTemplate.spec.resources.requests.storage"
    value = var.prometheus_volume_size
  }

  set {
    name  = "prometheus.prometheusSpec.replicas"
    value = var.prometheus_replicas
  }

  set {
    name  = "prometheus.alertmanagerSpec.replicas"
    value = var.alertmanager_replicas
  }

  set {
    name  = "alertmanager.configmapReload.enabled"
    value = true
  }

  values = [
    file("${path.module}/prometheus-extra-jobs.yaml")
  ]
}

resource helm_release prometheus_adapter {
  name       = "custom-metrics"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-adapter"

  set {
    name  = "prometheus.url"
    value = "http://prometheus-stack-kube-prom-prometheus.${kubernetes_namespace.monitoring.metadata.0.name}.svc"
  }

  values = [
    file("${path.module}/custom-metrics-list.yaml")
  ]
}
