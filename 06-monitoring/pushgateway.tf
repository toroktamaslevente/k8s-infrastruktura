resource helm_release prom-push-gateway {
  name       = "prometheus-push"
  namespace  = kubernetes_namespace.monitoring.metadata.0.name

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-pushgateway"
  version    = "1.12.0"
}