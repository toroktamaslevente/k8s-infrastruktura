resource helm_release missing_metrics {
  namespace  = "kube-system"
  name       = "missing-container-metrics"

  repository = "https://draganm.github.io/missing-container-metrics"
  chart      = "missing-container-metrics"
  version    = "0.1.1"
}